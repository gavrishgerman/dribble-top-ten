package com.attendify;

import com.attendify.dribbleapi.DribbbleApiClientImpl;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.Scanner;

public class TopTenApp {


    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);
        String dribbleName = scanner.next();

        Properties prop = new Properties();
        InputStream input = new FileInputStream("resources/config.properties");

        prop.load(input);

        DribbbleApiClientImpl dribbbleApiClient =
                new DribbbleApiClientImpl(60, prop.getProperty("access_token"), true);
        dribbbleApiClient.startTopTenTask(dribbleName);

        System.out.println("finished");
    }

}
