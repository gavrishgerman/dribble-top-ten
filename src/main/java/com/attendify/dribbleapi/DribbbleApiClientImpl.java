package com.attendify.dribbleapi;

import com.google.common.collect.HashMultiset;
import com.google.common.collect.Multiset;
import com.google.common.collect.Multisets;
import com.attendify.dribbleapi.handler.APIObserver;
import com.attendify.exception.ApiLimitExceedException;
import org.asynchttpclient.AsyncHttpClient;
import org.asynchttpclient.DefaultAsyncHttpClient;
import org.json.JSONArray;
import com.attendify.ratelimiter.QueryExecutor;
import com.attendify.ratelimiter.RateLimiter;
import com.attendify.ratelimiter.RateLimiterImpl;
import com.attendify.utils.JSONMappersUtils;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.*;
import java.util.stream.Collectors;

public class DribbbleApiClientImpl implements DribbbleApiClient {
    private static final int MAX_PER_PAGE_ENTETIES = 100;
    private AsyncHttpClient client = new DefaultAsyncHttpClient();
    private RateLimiter rateLimiter;
    private String accessToken;

    private boolean showProgress;

    private List<String> poison = new LinkedList<>();


    private ExecutorService sharedExecutionService;
    private Queue<String> allNames = new ConcurrentLinkedQueue<>();
    private BlockingQueue<List<String>> likersQueue;

    public DribbbleApiClientImpl(int permits, String accessToken, boolean showProgress) {
        APIObserver finishHandler = () -> {
            //stops main thread from busy waiting and prints out results
            System.out.println("TOP 10 LIKERS OVERALL:");
            for (String liker : getCurrentTopTen()) {
                System.out.println(liker);
            }

            sharedExecutionService.shutdownNow();
            //put poison pill to break out
            try {
                likersQueue.put(poison);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        };
        this.sharedExecutionService = Executors.newFixedThreadPool(permits);
        this.showProgress = showProgress;
        this.rateLimiter = new RateLimiterImpl(permits, finishHandler);
        this.accessToken = accessToken;
    }

    private void fillFollowersQueue(String userName, BlockingQueue<List<String>> followersQueue) {
        Runnable runnable = () -> {
            try {
                int page = 1;
                while (true) {
                    String query = "https://api.dribbble.com/v1/users/" + userName +
                            "/followers?access_token=" + accessToken + "&per_page=100&page=" + page;
                    QueryExecutor queryExecutor = new QueryExecutor(rateLimiter, client, query);
                    JSONArray jsonArray = queryExecutor.executeQuery();
                    List<String> followers = JSONMappersUtils.mapFollowersToList(jsonArray);
                    followersQueue.put(followers);

                    if (followers.size() < MAX_PER_PAGE_ENTETIES) {
                        break;
                    }
                    page++;
                }
            } catch (InterruptedException | ExecutionException | ApiLimitExceedException e) {
                e.printStackTrace();
            }
        };
        sharedExecutionService.execute(runnable);

    }

    private void fillShotsQueue(String userName, BlockingQueue<List<Integer>> shots) {
        sharedExecutionService.execute(() -> {
            int page = 1;
            try {
                while (true) {
                    String query = "https://api.dribbble.com/v1/users/" + userName +
                            "/shots?access_token=" + accessToken + "&per_page=100&page=" + page;
                    QueryExecutor queryExecutor = new QueryExecutor(rateLimiter, client, query);
                    JSONArray jsonArray = queryExecutor.executeQuery();
                    List<Integer> followers = JSONMappersUtils.mapShotsToList(jsonArray);
                    shots.put(followers);
                    if (followers.size() < MAX_PER_PAGE_ENTETIES) {
                        break;
                    }
                    page++;
                }
            } catch (InterruptedException | ExecutionException | ApiLimitExceedException e) {
                e.printStackTrace();
            }
        });
    }

    private List<String> getCurrentTopTen() {
        Multiset<String> multiset = HashMultiset.create(allNames);
        return Multisets.copyHighestCountFirst(multiset).elementSet()
                .stream().limit(10).collect(Collectors.toList());
    }

    private void fillLikersQueue(Integer shotId, BlockingQueue<List<String>> likers) {
        sharedExecutionService.execute(() -> {
            int page = 1;
            try {
                while (true) {
                    String query = "https://api.dribbble.com/v1/shots/" + shotId +
                            "/likes?access_token=" + accessToken + "&per_page=100&page=" + page;
                    QueryExecutor queryExecutor = new QueryExecutor(rateLimiter, client, query);
                    JSONArray jsonArray = queryExecutor.executeQuery();
                    List<String> likersResult = JSONMappersUtils.mapLikersToList(jsonArray);
                    likers.put(likersResult);
                    if (likersResult.size() < MAX_PER_PAGE_ENTETIES) {
                        break;
                    }
                    page++;
                }
            } catch (InterruptedException | ExecutionException | ApiLimitExceedException e) {
                e.printStackTrace();
            }
        });
    }

    public void startTopTenTask(String dribbleName) {
        BlockingQueue<List<String>> followersQueue = new LinkedBlockingQueue<>();
        BlockingQueue<List<Integer>> shotsQueue = new LinkedBlockingQueue<>();
        likersQueue = new LinkedBlockingQueue<>();
        fillFollowersQueue(dribbleName, followersQueue);

        ExecutorService followerService = Executors.newSingleThreadExecutor();
        ExecutorService shotsService = Executors.newSingleThreadExecutor();

        //listening to followers queue and filling shots queue
        followerService.execute(() -> {
            while (true) {
                try {
                    List<String> followers = followersQueue.take();
                    for (String follower : followers) {
                        fillShotsQueue(follower, shotsQueue);
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        //listening to shots queue and filling likers queue
        shotsService.execute(() -> {
            while (true) {
                try {
                    List<Integer> shots = shotsQueue.take();
                    for (Integer shot : shots) {
                        fillLikersQueue(shot, likersQueue);
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        while (true) {
            try {
                List<String> likers = likersQueue.take();
                if (likers.equals(poison)) {
                    break;
                }
                allNames.addAll(likers);
                if (showProgress) {
                    printCurrentTopTen();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }

    private void printCurrentTopTen() {
        System.out.println("Top10Liker so far: ");
        getCurrentTopTen().forEach(System.out::println);
        System.out.println();
    }
}
