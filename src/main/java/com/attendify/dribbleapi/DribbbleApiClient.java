package com.attendify.dribbleapi;

public interface DribbbleApiClient {

    /***
     * Start dribble client to processing top ten likers
     * @param userName
     */
    void startTopTenTask(String userName);
}
