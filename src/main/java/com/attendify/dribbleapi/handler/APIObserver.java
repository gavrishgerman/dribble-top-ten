package com.attendify.dribbleapi.handler;

public interface APIObserver {
    /**
     * Inform listeners when all tasks have been finished
     */
    void informFinish();
}
