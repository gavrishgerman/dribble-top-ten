package com.attendify.ratelimiter;

/**
 * Interface for acquiring lock. Before every execution
 * thread acquire it's lock to make sure thread can
 * execute ot's task
 */
public interface RateLimiter {
    void acquire() throws InterruptedException;

    /**
     * Release all locks
     * @param resetTime - time when all available locks will be released
     */
    void scheduleNewReleaseTime(long resetTime);
}
