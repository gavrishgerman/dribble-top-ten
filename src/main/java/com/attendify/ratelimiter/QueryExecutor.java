package com.attendify.ratelimiter;

import com.attendify.exception.ApiLimitExceedException;
import org.asynchttpclient.AsyncHttpClient;
import org.asynchttpclient.ListenableFuture;
import org.asynchttpclient.Response;
import org.json.JSONArray;
import org.json.JSONException;

import java.util.concurrent.ExecutionException;

public class QueryExecutor {

    private RateLimiter limiter;
    private AsyncHttpClient client;
    private String query;

    public QueryExecutor(RateLimiter limiter, AsyncHttpClient client, String query) {
        this.limiter = limiter;
        this.client = client;
        this.query = query;
    }

    public JSONArray executeQuery() throws ApiLimitExceedException, InterruptedException, ExecutionException {
        JSONArray result = null;
        Response response = null;

        try {
            //acquire lock, if cannot stop current thread
            limiter.acquire();

            ListenableFuture<Response> execute = client.prepareGet(query).execute();
            response = execute.get();

            //set new reset time
            String resetInEpoch = response.getHeader("X-RateLimit-Reset");
            long resetTime = Long.parseLong(resetInEpoch) * 1000;
            limiter.scheduleNewReleaseTime(resetTime);


            String responseBody = response.getResponseBody();

            result = new JSONArray(responseBody);
        } catch (JSONException e) {
            String responseBody = response.getResponseBody();
            if (responseBody.equals("API rate limit exceeded.")) {
                throw new ApiLimitExceedException();
            } else {
                e.printStackTrace();
            }
        }
        return result;
    }
}
