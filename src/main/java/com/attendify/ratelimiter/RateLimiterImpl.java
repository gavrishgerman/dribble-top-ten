package com.attendify.ratelimiter;

import com.attendify.dribbleapi.handler.APIObserver;

import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Once per minute releases all tasks in fair order
 */
public class RateLimiterImpl implements RateLimiter {

    private Semaphore semaphore;
    private volatile long resetTime = 0;
    private APIObserver finishHandler;
    private AtomicInteger countAcquired = new AtomicInteger(0);


    public RateLimiterImpl(int permits, APIObserver finishHandler) {
        this.semaphore = new Semaphore(permits, true);
        this.finishHandler = finishHandler;
    }

    @Override
    public void acquire() throws InterruptedException {
        semaphore.acquire();
        countAcquired.incrementAndGet();
    }

    @Override
    public void scheduleNewReleaseTime(long resetTime) {
        synchronized (this) {
            if (this.resetTime != resetTime) {
                this.resetTime = resetTime;
                long now = System.currentTimeMillis();
                long delay = resetTime - now;
                Executors.newSingleThreadScheduledExecutor().schedule(() -> {
                    semaphore.release(countAcquired.get());
                    countAcquired.set(0);
                    //if nobody waits -> informing finish
                    if (semaphore.getQueueLength() == 0) {
                        if (finishHandler != null) {
                            finishHandler.informFinish();
                        }
                    }
                }, delay, TimeUnit.MILLISECONDS);
            }
        }
    }
}
