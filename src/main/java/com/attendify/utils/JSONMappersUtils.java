package com.attendify.utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedList;
import java.util.List;

public class JSONMappersUtils {

    private JSONMappersUtils() {
    }

    public static List<String> mapFollowersToList(JSONArray followers) {
        List<String> res = new LinkedList<String>();
        for (int i = 0; i < followers.length(); i++) {
            String username = null;
            try {
                username = ((JSONObject) followers.get(i)).getJSONObject("follower").getString("username");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            res.add(username);
        }
        return res;
    }

    public static List<Integer> mapShotsToList(JSONArray followers) {
        List<Integer> res = new LinkedList<Integer>();
        for (int i = 0; i < followers.length(); i++) {
            int id = 0;
            try {
                id = ((JSONObject) followers.get(i)).getInt("id");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            res.add(id);
        }
        return res;
    }

    public static List<String> mapLikersToList(JSONArray followers) {
        List<String> res = new LinkedList<String>();
        for (int i = 0; i < followers.length(); i++) {
            String names = null;
            try {
                names = ((JSONObject) followers.get(i)).getJSONObject("user").getString("name");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            res.add(names);
        }
        return res;
    }


}
