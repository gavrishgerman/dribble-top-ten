import com.attendify.dribbleapi.DribbbleApiClientImpl;
import org.asynchttpclient.AsyncHttpClient;
import org.asynchttpclient.DefaultAsyncHttpClient;
import org.junit.Before;
import org.junit.Test;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class DribbleApiTests {

    private AsyncHttpClient client;
    private String accessToken;

    @Before
    public void init() throws IOException {

        Properties prop = new Properties();
        InputStream input = new FileInputStream("resources/config.properties");

        prop.load(input);

        accessToken = prop.getProperty("access_token");
        client = new DefaultAsyncHttpClient();
    }

    @Test
    public void followersQueueTest() throws InterruptedException {
        DribbbleApiClientImpl dribbbleApiClient = new DribbbleApiClientImpl(60, accessToken, true);

        dribbbleApiClient.startTopTenTask("JulienRenvoye");
    }
}
