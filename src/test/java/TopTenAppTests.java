import com.google.common.collect.HashMultiset;
import com.google.common.collect.Multiset;
import com.google.common.collect.Multisets;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONException;
import org.junit.Test;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.util.*;

public class TopTenAppTests {


    @Test
    public void countTest() {
        List<String> names = new LinkedList<>(Arrays.asList("name1", "name1", "name2", "name2", "name2", "name3",
                "name1", "name1", "name2", "name2", "name2", "name3"));

        Multiset<String> multiset = HashMultiset.create();
        multiset.addAll(names);

        Multisets.copyHighestCountFirst(multiset).elementSet()
                .stream().limit(10).forEach(System.out::println);

    }

    @Test
    public void headersParsingTest() throws IOException, JSONException, ParseException, InterruptedException {
        CloseableHttpClient client = HttpClientBuilder.create().build();

        Properties prop = new Properties();
        InputStream input = new FileInputStream("resources/config.properties");

        prop.load(input);

        HttpResponse response = client.execute(new HttpGet("https://api.dribbble.com/v1/users/" + "trentkokic" +
                "/shots?access_token=" + prop.getProperty("access_token") + "&per_page=100&page=" + 1));

        Header limitRemaining = response.getFirstHeader("X-RateLimit-Remaining");
        Header resetInEpoch = response.getFirstHeader("X-RateLimit-Reset");

        Date restDate = new Date(Long.parseLong(resetInEpoch.getValue()) * 1000);

        long restTimeLong = restDate.getTime();
        long time = new Date().getTime();

        long millis = restTimeLong - time;
        System.out.println(millis);
        Thread.sleep(millis);

        System.out.println(limitRemaining.getName() + ": " + limitRemaining.getValue());
        System.out.println(resetInEpoch.getName() + ": " + resetInEpoch.getValue());

        System.out.println(restDate);

    }
}
